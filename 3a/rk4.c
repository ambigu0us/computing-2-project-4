#include <stdio.h>
#include <math.h>

/** Code to solve the Initial Value Problem using Euler Method for h = 0.1.
**
** The ODE is given by:
**
**                        dy/dx = -2xy/1+x^2; y(0) = 1.0
**
**/

double func_y, a_y;

double ode(double x, double y) {
    func_y = (-2*x*y)/(1+pow(x,2)); //add y0*h to y0, to calculate the next value of y for the step in x
    return func_y;
}

double k1(double x, double y, double h) {
    return h*ode(x,y);
}

double k2(double x, double y, double h) {
    return h*ode( x+h/2 , y+(k1(x,y,h)/2) );
}

double k3(double x, double y, double h) {
    return h*ode( x+h/2 , y+(k2(x,y,h)/2) );
}

double k4(double x, double y, double h) {
    return h*ode( x+h , y+k3(x,y,h) );
}


double analytical(double x) {
    a_y = 1/(1+pow(x,2)); //add y0*h to y0, to calculate the next value of y for the step in x
    return a_y;
}

int main(void){//start main program

    //format table
    printf("h\t\tApprox y(1)\tActual y(1)\t|Error|/h\n");

    //define the starting variables
    double xi = 0.0;
    double xe = 1.0;
    double yini = 1.0;
    double h[] = {0.04, 0.02, 0.01, 0.005, 0.0025, 0.00125};
    double X, y0, yf,ya,error,H;
    int i=1,j,numofsteps;

    //hsize contains number of stepsizes to test
    int hsize = sizeof(h)/sizeof(h[0]);

    //calculate analytical answer
    ya = analytical(xe);

    //start for loop to control step size
    for (i = 0; i < hsize; i++) {

        //use H as step size
        H=h[i];

        y0 = yini;

        //calculate number of steps between xe and xi for the step size
        numofsteps = (xe - xi) / H;

        //using a for loop instead of the while loop because while loops with floats are interesting...
        for (j=0;j<numofsteps;j++){

            //define X as the initial X value + step size * j (replaces the X+=h)
            X=xi+(H*j);

            //add y0*h to y0, to calculate the next value of y for the step in x
            yf = y0 + ((k1(X,y0,H)+2*k2(X,y0,H)+2*k3(X,y0,H)+k4(X,y0,H))/6);

            //make y0 equal to yf
            y0 = yf;
            //printf("X %f\ty %f\tk1 %f\tk2 %f\tk3 %f\tk4 %f\ttot %f\n",X, yf, k1(X,y0,H),k2(X,y0,H),k3(X,y0,H),k4(X,y0,H),(k1(X,y0,H)+(2*k2(X,y0,H))+(2*k3(X,y0,H))+k4(X,y0,H))/6);//print the results for this loop
        }

        //calculate percentage error against analytical value
        error=(yf-ya)/ya;

        //if the error is -ve, flip the sign
        if (error<0) {
            error=error*-1;
        }

        //print the line of results for each step size
        printf("%f\t%f\t%f\t%f\n", H, yf, ya, error/ya);//print the results for this loop
    }
    return 0;
}

