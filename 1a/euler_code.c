#include <stdio.h>
#include <math.h>

/** Code to solve the Initial Value Problem using Euler Method for h = 0.1.
**
** The ODE is given by:
**
**                        dy/dx = -2xy/1+x^2; y(0) = 1.0
**
**/

double func_y, a_y;

double ode(double x, double y) {
    func_y = (-2*x*y)/(1+pow(x,2)); //add y0*h to y0, to calculate the next value of y for the step in x
    return func_y;
}

double analytical(double x) {
    a_y = 1/(1+pow(x,2)); //add y0*h to y0, to calculate the next value of y for the step in x
    return a_y;
}

int main(void){//start main program

    //format table
    printf("h\t\tApprox y(1)\tActual y(1)\t|Error|/h\n");

    //define the starting variables
    double xi = 0.0;
    double xe = 1.0;
    double yini = 1.0;
    double h[] = {0.04, 0.02, 0.01, 0.005, 0.0025, 0.00125};
    double X, y0, yf,ya,error;
    int i,j,numofsteps;

    //hsize contains number of stepsizes to test
    int hsize = sizeof(h)/sizeof(h[0]);

    //calculate analytical answer
    ya = analytical(xe);

    //start for loop to control step size
    for (i = 0; i < hsize; i++) {

        y0 = yini;

        //calculate number of steps between xe and xi for the step size
        numofsteps = (xe - xi) / h[i];

        //using a for loop instead of the while loop because while loops with floats are interesting...
        for (j=0;j<numofsteps;j++){

            //define X as the initial X value + step size * j (replaces the X+=h)
            X=xi+(h[i]*j);

            //perform the euler calculation
            yf = y0 + h[i]*ode(X,y0);

            //make y0 equal to yf
            y0 = yf;
        }

        //calculate percentage error against analytical value
        error=(yf-ya)/ya;

        //if the error is -ve, flip the sign
        if (error<0) {
            error=error*-1;
        }

        //print the line of results for each step size
        printf("%f\t%f\t%f\t%f\n", h[i], yf, ya, error/h[i]);//print the results for this loop
    }
    return 0;
}

