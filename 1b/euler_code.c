#include <stdio.h>
#include <math.h>

/** Code to solve the Initial Value Problem using Euler Method for h = 0.1.
**
** The ODE is given by:
**
**                        dy/dx = -2xy/1+x^2; y(0) = 1.0
**
**/

double func_y;

double ode(double x, double y, double h) {
    func_y = h*(pow(x,2)+pow(y,2)); //add y0*h to y0, to calculate the next value of y for the step in x
    return func_y;
}

int main(void){//start main program

    //format table
    printf("h\t\tApprox y(1)\tActual y(1)\t|Error|/h\n");

    //define the starting variables
    double xi = 0.0;
    double xe = 1.0;
    double yini = 1.0;
    double h[] = {0.1, 0.02, 0.005};
    double X, y0, yf,ya,error;
    int i,j,numofsteps;

    //hsize contains number of stepsizes to test
    int hsize = sizeof(h)/sizeof(h[0]);

    //calculate analytical answer
    //ya = analytical(xe);

    //start for loop to control step size
    for (i = 0; i < hsize; i++) {
        printf("\nStep size: %f\n\n", h[i]);

        y0 = yini;

        //calculate number of steps between xe and xi for the step size
        numofsteps = (xe - xi) / h[i];

        //using a for loop instead of the while loop because while loops with floats are interesting...
        for (j=0;j<numofsteps;j++){

            //define X as the initial X values + step size * j (replaces the X+=h)
            X=xi+(h[i]*j);

            //add y0*h to y0, to calculate the next value of y for the step in x
            yf = y0 + ode(X,y0,h[i]);

            //make y0 equal to yf
            y0 = yf;

            //print the x and y values
            printf("%f\t%f\n",X+h[i], yf);
        }


    }
    return 0;
}

